import React from 'react';
import style from './MainBlock.module.scss';

//import ReactLogo from '../../logo.png';

const MainBlock = () => {
    return(
        <div className={style.cover}>
            <div className={style.wrap}>
                <p className={style.descr}>Content will be here</p>
            </div>
        </div>
    )
}

export default MainBlock;