import React from 'react';
import style from './HeroBlock.module.scss';

//import ReactLogo from '../../logo.png';

const HeroBlock = () => {
    return(
        <div className={style.cover}>
            <div className={style.wrap}>
                <h1 className={style.header}>Учите слова онлайн</h1>
                <p className={style.descr}>Воспользуйтесь карточками для запоминания и пополнения активныйх словарных запасов</p>
            </div>
        </div>
    )
}

export default HeroBlock;