import React from 'react';
import style from './HeaderBlock.module.scss';

import { ReactComponent as ReactLogoSvg } from '../../logo.svg';


const menu = ['Home', 'Cards', 'Text'];
const HeaderBlock = () => {
    return(
        <header>
            <a href="#">
                <ReactLogoSvg />
            </a>
            <ul className={style.nav}>
            {menu.map(item => <li><a href="#">{item}</a></li>)}
            </ul>
        </header>
    )
}

export default HeaderBlock;