import React from 'react';
import style from './FooterBlock.module.scss';

const FooterBlock = () => {
    return(
        <footer className={style.footer}>
            All right reserved &copy; 2020
        </footer>
    )
}

export default FooterBlock;