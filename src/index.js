import React from 'react';
import ReactDom from 'react-dom';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';


import HeaderBlock from './components/HeaderBlock';
import HeroBlock from './components/HeroBlock';
import MainBlock from './components/MainBlock';
import FooterBlock from './components/FooterBlock';
import './index.css';
import 'fontsource-roboto';

//const el = React.createElement();
const App = () => {
  return(
    <>
      <HeaderBlock />
      <HeroBlock />
      <MainBlock />
      <FooterBlock />
    </>
  );
}

ReactDom.render(<App />, document.getElementById('root'));